# Government Site Builder 11 

**Dieses Projekt spiegelt den aktuellen Stand der Entwicklung des GSB 11 wider. Es enthält Funktionen und Features, die noch nicht durch einen Abnahmetest geprüft wurden.**

**Um die aktuelle freigegebene Releaseversion des GSB 11 zu erhalten, die künftig quartalsweise Abnahmetests unterzogen wird, wenden Sie sich bitte an gsb11-entwicklung@itzbund.de.**

## Der GSB 11 ist das neue Content Management System der Bundesverwaltung.

Der Government Site Builder 11 (GSB 11) ist eine Content Management Lösung zur Erstellung, Qualitätssicherung, Freigabe und Veröffentlichung von Inhalten im Internet, Intranet und Extranet. Mit dem GSB 11 steht ein Produkt für die effiziente Erstellung von responsiven Webauftritten zur Verfügung.

Im Rahmen der IT-Dienstekonsolidierung Bund soll mit der IT-Lösung GSB 11 eine Vereinheitlichung der Dienstelandschaft in der Bundesverwaltung ermöglicht werden. Der GSB ab der Version 11 basiert auf dem OpenSource-Produkt TYPO3 (Version 12 LTS) und bietet „out-of-the-box“ flexible, vorkonfigurierte Module für behördentypische Anforderungen an ein modernes (Web) Content Management System (CMS). In Abhängigkeit der individuellen Anforderungen können einzelne Bausteine aus der Standard-Distribution des GSB 11 genutzt und spezifisch konfiguriert werden.

Der GSB 11 berücksichtigt die technischen Vorgaben der BITV und unterstützt Redaktionen bei der Erstellung von barrierefreien Webinhalten.

## Contribution

Das Entwicklungsteam des GSB 11 begrüßt alle Contributions zum GSB 11. Bitte folgen Sie unserem [Contribution Guide](https://gitlab.opencode.de/bmi/government-site-builder-11/extensions/gitlab-profile/-/blob/main/CONTRIBUTING.md). Alle Dokumentationen zur Entwicklung sind in englischer Sprache.

## GSB 11 Extensions

Alle GSB 11 Extensions finden Sie unter: https://gitlab.opencode.de/bmi/government-site-builder-11/extensions

## Start der Entwicklung

Für das Setup des GSB 11 für die Mandatenentwicklung beginnen Sie bitte mit der Installation des [GSB 11 Sitepackage Kickstarter](https://gitlab.opencode.de/bmi/government-site-builder-11/extensions/gsb-sitepackage-kickstarter).

Informationen zu den einzelnen GSB 11 Erweiterungen für TYPO3 entnehmen Sie bitte der jeweiligen README.md im Repository.

## License

Lizensiert unter GPL 3.0 or later.
